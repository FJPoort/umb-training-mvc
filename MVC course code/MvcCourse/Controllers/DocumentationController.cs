﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcCourse.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace MvcCourse.Controllers
{
    public class DocumentationController : RenderMvcController
    {
        // GET: DocumentationController
        public ActionResult Index(RenderModel model)
        {
            //get the related landing pages
            var ids = Services.RelationService.GetByParentId(model.Content.Id)
                            .Select(x => x.ChildId);

            //fetch them as nodes
            var products = Umbraco.TypedContent(ids).OfType<LandingPage>();

            // create a new view model
            var viewmodel = new DocumentationViewModel(model.Content)
            {
                LandingPages = products
            };

            return CurrentTemplate(viewmodel);
        }
    }
}