﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcCourse.Helper;
using MvcCourse.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace MvcCourse.Controllers
{
    public class DocumentationFormController : SurfaceController
    {
        [ChildActionOnly]
        public ActionResult Render(Documentation model)
        {
            var docFormModel = new DocumentationFormModel
            {
                Name = model.Name,
                BodyText = model.BodyText.ToString()
            };
            return PartialView(docFormModel);
        }

        [HttpPost]
        public ActionResult Submit(DocumentationFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            var curPageId = CurrentPage.Id;
            var content = Services.ContentService.GetById(curPageId);
            var bodyTextProp = Documentation.GetModelPropertyType(d => d.BodyText);

            content.Name = model.Name;
            content.SetValue(bodyTextProp.PropertyTypeAlias, model.BodyText);

            if (model.Images.HasFiles() && model.Images.ContainsImages())
            {
                var imagesProperty = Documentation.GetModelPropertyType(d => d.Images);
                var folderId = content.GetValue<int>(imagesProperty.PropertyTypeAlias);
                var mediaService = Services.MediaService;

                if (folderId <= 0)
                {
                    var folder = mediaService.CreateMedia(model.Name, -1, Folder.ModelTypeAlias);
                    mediaService.Save(folder);
                    folderId = folder.Id;
                    content.SetValue(imagesProperty.PropertyTypeAlias, folderId);
                }

                foreach (var file in model.Images)
                {
                    if (file.IsImage())

                    {
                        var mediaImage = mediaService.CreateMedia(file.FileName, folderId, Image.ModelTypeAlias);
                        var umbFileProp = Image.GetModelPropertyType(i => i.UmbracoFile);

                        mediaImage.SetValue(umbFileProp.PropertyTypeAlias, file);
                        mediaService.Save(mediaImage);
                    }
                }
            }

            Services.ContentService.SaveAndPublishWithStatus(content);


            return RedirectToCurrentUmbracoPage();
        }
    }
}